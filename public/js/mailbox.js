
function showsbdetails() {
    $(".sblabel").each((i, lbl) => {
        var label = document.createElement("span");
        label.innerHTML = `&nbsp;&nbsp;-&nbsp;&nbsp;${lbl.title}`
        lbl.append(label);
    });
}

function hidesbdetails() {
    $(".sblabel > span").remove();
}

$(document).ready(() => {
    $("#mainmenu").click(() => {
        let elems = $(".main.sidebar")
        if (elems.attr("aria-expanded") === "false") {
            elems.animate({width: "7em"}, showsbdetails()).attr("aria-expanded", "true");
        } else {
            elems.animate({width: "1.5em"}, hidesbdetails()).attr("aria-expanded", "false");
        }
    });

    $(".sblabel:not(.settings)").click((e) => {
        let p = $(e.delegateTarget).parent();
        if (p.attr("data-selected") === "false") {
            let datatoload = e.delegateTarget.title;
            $(".sbli.main").attr("data-selected", "false");
            p.attr("data-selected", "true");
            // Receiving data
            let token = document.cookie
                .split("; ")
                .find(row => row.startsWith("Token"))
                .split("=")[1];
            $(".maildisp.sidebar")
                .load("/v1/api/listmail", {authentication: token, toload: datatoload })
                .show()
                .attr("aria-hidden", "false");
        }
    });
  });