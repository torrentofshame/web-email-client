$(".mailsblabel").click((e) => {
    let p = $(e.delegateTarget).parent();
    if (p.attr("data-selected") === "false") {
        $(".maildisp.sbli").attr("data-selected", "false");
        p.attr("data-selected", "true");
        let token = document.cookie
            .split("; ")
            .find(row => row.startsWith("Token"))
            .split("=")[1];
        $("main").load(`/v1/api/viewemail/${p.attr("id")}`, {authentication: token});
    }
});