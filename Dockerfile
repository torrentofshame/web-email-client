FROM node:12-alpine
LABEL maintainer="contact@simon.weizman.us"

RUN mkdir -p /home/node/app/node_modules && chown -R node:node /home/node/app

WORKDIR /home/node/app

COPY package*.json ./

USER node

RUN npm install

COPY --chown=node:node . .

EXPOSE 8080

ENTRYPOINT [ "npm", "run", "dev:server" ]