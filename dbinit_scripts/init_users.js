db.createCollection("users", {
    validator: {$jsonSchema: {
        bsonType: "object",
        required: ["id", "email", "password"],
        properties: {
            id: {
                bsonType: "string",
                description: "must be a string and is required"
            },
            email: {
                bsonType: "string",
                pattern: "@.*\.com$",
                description: "must be a string and match the regex pattern"
            },
            password: {
                bsonType: "string",
                description: "must be a string and is required"
            },
        }
    }},
    validationAction: "warn"
})