const jwt = require("jsonwebtoken");
const express = require("express");
const router = express.Router();

router.use( (req, res, next) => next());

router.get("/", (req, res) => {
    res.send("API Index");
});

/*** Public Data API ***/

router.get("/test", (req, res) => {
    res.send("testing u poop");
});

/*** Authenticate Tokens ***/

function authToken(req, res, next) {
    let token = req.body.authentication;
    if (token == null) return res.sendStatus(401);

    jwt.verify(token, process.env.TOKEN_SECRET, (err, uname) => {
        console.log(err);
        if (err) return res.sendStatus(403);
        req.user = uname;
        next();
    });

}

/*** Temporary list of "emails" ***/
var mailobjlst = [
    {"id":0,"timestamp":"1:22pm", "sender":"NoReply Weizman", "subject":"Lorem ipsum", "peek":"Lorem ipsum dolor sit amet"},
    {"id":1,"timestamp":"1:22pm", "sender":"NoReply Weizman", "subject":"Lorem ipsum2", "peek":"Lorem ipsum dolor sit amet"},
    {"id":2,"timestamp":"1:22pm", "sender":"NoReply Weizman", "subject":"Lorem ipsum3", "peek":"Lorem ipsum dolor sit amet"}
]

/*** Private API ***/

router.post("/listmail", authToken, (req, res) => {
    let toload = req.body.toload;
    res.render("maillist", {mailtitle: toload, mailobjlst: mailobjlst});
});

router.param("emailid", (req, res, next, emailid) => {
   if (emailid != null) {
       req.emailid = emailid;
       next();
   }
});

/*** Fake email Object ***/
var testemailobj = { //Gonna need to parse the actual mime data
    "id": 0, "timestamp": "4:20pm",
    "sender": "NoReply Weizman", "sender_email": "noreply@weizman.us",
    "receiver_email":"blah@blah.com",
    "body":"<html><head></head><body><p>Lorem ipsum dolor sit amet do do the do</p></body></html>"
}

router.post("/viewemail/:emailid", authToken, (req, res) => {
    if (req.user) { // Check if email belongs to user
        res.render("emailviewer", {email: testemailobj})
    }
});


module.exports = router;