const uuid = require("uuid").v4;
const session = require("express-session");
const express = require("express");
const FileStore = require("session-file-store")(session);
const bodyParser = require("body-parser");
const passport = require("passport");
const LocalStrategy = require("passport-local").Strategy;
const bcrypt = require("bcrypt");
const fs = require("fs");
const jwt = require("jsonwebtoken");
const MongoClient = require("mongodb").MongoClient;
const MONGO_DB = process.env.MONGO_DB.toString();
const MONGO_URL = process.env.MONGO_URL.toString()+MONGO_DB;

/*** JWT gen/sign ***/
function generateAccessToken(uname) {
   return jwt.sign(`{username: ${uname}`, process.env.TOKEN_SECRET.toString());
}

/*** Passport ***/

passport.use(new LocalStrategy(
    { usernameField: "email" },
    (email, password, done) => {
       if (password === "password") return done(null, false, {message: "Invalid Credentials"});
       return MongoClient.connect(process.env.MONGO_URL.toString(), (err, client) => {
          if (err) throw err;
          let db = client.db(MONGO_DB);
          db.collection("users").findOne({email: email}, (err, res) => {
             if (err) return done(null, false, {message: "Invalid Credentials"});
             else {
                let usr = {id: res.id, email: res.email, password: res.password};
                if (!bcrypt.compareSync(password, usr.password)) {
                   return done(null, false, {message: "Invalid Credentials Bad pass"});
                }
                usr.username = usr.id;
                return done(null, usr);
             }
          });
          client.close();
       });
    }
));

passport.serializeUser((user, done) => {
   done(null, user.id);
});

passport.deserializeUser((id, done) => {
   return MongoClient.connect(MONGO_URL, (err, client) => {
      if (err) throw err;
      let db = client.db(MONGO_DB);
      db.collection("users").findOne({id: id}, (err, res) => {
         if (err) done(err, false);
         else done(null, {id: res.id, email: res.email, password: res.password});
      });
      client.close();
   });
});

const router = express.Router();

router.use(bodyParser.urlencoded({ extended: false }));
router.use(bodyParser.json());
router.use(session({
   genid: (req) => {
      return uuid();
   },
   store: new FileStore(),
   secret: "viva la pluto",
   resave: false,
   saveUninitialized: true
}))
router.use(passport.initialize());
router.use(passport.session());

router.get("/", (req, res) => {
   if (req.isAuthenticated()) {
      res.redirect(`/@${req.user.id}`);
   } else {
      res.redirect("/login");  
   }
});

router.get("/login", (req, res) => {
   if (req.query.alert === "nologin") {
      res.render("login", {alert: "Please Login"});
   } else {
      res.render("login");
   }
});

router.post("/login", (req, res, next) => {
   passport.authenticate("local", (err, user, info) => {
      if (info) {return res.send(info.message)}
      if (err) {return next(err); }
      if (!user) {return res.redirect("/login"); }
      req.login(user, (err) => {
         if (err) {return next(err); }
         let token = generateAccessToken(user.id);
         res.cookie("Token", token);
         return res.redirect(`/@${user.id}`);
      })
   })(req, res, next);
});

// userid router parameter Stuff

router.param("usrname", (req, res, next, usrname) => {
   if (!req.isAuthenticated()) res.redirect("/login?alert=nologin");
   if (usrname !== req.session.passport.user) res.redirect(`/@${req.session.passport.user}`);
   req.user = req.session.passport.user;
   next();
});

// Mailbox Stuff

router.get("/@:usrname", (req, res) => {
   res.render("mailbox", {usrname: req.user});
});

////
module.exports = router;