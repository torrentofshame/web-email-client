# TorrentofMail
[![time tracker](https://wakatime.com/badge/gitlab/torrentofshame/web-email-client.svg)](https://wakatime.com/badge/gitlab/torrentofshame/web-email-client)

I'm basically creating a email client that can be used from the web. Think a knock-off Gmail or Outlook.

Created using Node.js running with Express.js as the framework and pug.js as a view engine.

**Work in Progress**