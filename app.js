const helmet = require("helmet");
const express = require("express");
const dotenv = require("dotenv").config();
const app = express();
const MongoClient = require("mongodb").MongoClient;
const MONGO_DB = process.env.MONGO_DB.toString();
const MONGO_URL = process.env.MONGO_URL.toString()+MONGO_DB;
const bcrypt = require("bcrypt");
const host = "0.0.0.0";
const port = 8080;

/*** Init root admin user in MongoDB users collection ***/
MongoClient.connect(MONGO_URL, (err, client) => {
    if (err) throw err;
    let db = client.db(MONGO_DB);
    db.collection("users").findOne({id: "root"}, (err, res) => {
        if (err) throw err;
        if (res === null) {
            db.collection("users").insertOne({
                id: "root",
                email: process.env.ROOT_ADMIN_USERNAME.toString(),
                password: bcrypt.hashSync(process.env.ROOT_ADMIN_PASSWORD.toString(), 10)
            }, (e, r) => {
                if (e) throw e;
                client.close();
            });
        }
    });
});

app.set("views", "./views");
app.set("view engine", "pug");

// routes
var routes = require("./routes/route");
var v1api = require("./routes/v1/api");
// SASS
var sassMiddleware = require("node-sass-middleware");
////
app.use(helmet());
app.use(sassMiddleware({
    src: "public/stylesheets/scss",
    dest: "public/stylesheets/css",
    debug: false,
    outputStyle: "compressed",
    prefix: "/stylesheets/css"
}));
app.use(express.static("public"));
app.use("/", routes);
app.use("/v1/api", v1api);

app.use(function (req, res, next) {
    res.status(404).send("Sorry can't find that!")
})

app.listen(port, host, () => console.log(`listening at http://${host}:${port}`));

module.exports = app;
